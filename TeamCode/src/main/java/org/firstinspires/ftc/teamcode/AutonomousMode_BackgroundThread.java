package org.firstinspires.ftc.teamcode;

/**
 * Created by nicolas on 1/28/18 in ftc_app.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/ftc_app
 * Contact: nico@walbots.com, team@walbots.com
 */


import org.firstinspires.ftc.teamcode.Walbots7175.internal.AutonomousAction;
import org.firstinspires.ftc.teamcode.Walbots7175.internal.AutonomousController;


/**
 * The class AutonomousMode_BackgroundThread is running the autonomous mode as a background thread.
 * It is managing the creation and execution of all various actions (AutonomousFunctions) that have
 * to be run during the autonomous period.
 *
 * The autonomous mode will be initialized in the AutonomousMode class.
 */
public class AutonomousMode_BackgroundThread implements Runnable
{
    private HardwareController hardware;            //For accessing the robot's hardware
    private boolean isBlueAlliance;                 //Indicating in which alliance we are in
    private boolean isFrontPlace;                   //Indicating on which place we are starting

    private boolean isAutonomousFinished = false;   //Indicating if all actions are completed

    /**
     * Initializing an AutonomousThread with the current AutonomousMode
     *
     * @param currentAutonomousMode The currently running autonomous mode
     */
    AutonomousMode_BackgroundThread(AutonomousMode currentAutonomousMode)
    {
        this.hardware = currentAutonomousMode.hardware;
        this.isBlueAlliance = currentAutonomousMode.isBlueAlliance();
        this.isFrontPlace = currentAutonomousMode.isFrontPlace();
    }

    /**
     * The run() method is part of the Thread life-cycle and gets called after the Thread was
     * started. Everything should already be initialized for running this autonomous mode in the
     * class AutonomousMode!
     *
     * In this case the run() method only runs once and will be directly returned afterwards. All
     * actions as AutonomousFunctions are instantiated and specified so that the
     * AutonomousController runs through them step by step completing all actions we want to do
     * during autonomous.
     */
    @Override
    public final void run()
    {
        //Check if the autonomous mode is done, if yes return
        if (isAutonomousFinished)
        {
            return;
        }


        //Run all functions
        //TODO: Run all AutonomousFunctions (Autonomous_***) for this AutonomousMode

//        //Autonomous Functions
//        AutonomousAction[] functions = new AutonomousAction[2];
//
//        //Drive Forward
//        float[] drivingPowers = {1f, 1f};
//        functions[0] = new AutonomousSample_Driving(hardware, "Driving forward", drivingPowers);
//        functions[0].timeToWaitAfterFinished = 1000;    //Drive forward for 1 sec
//
//        //Color Sensor
//        functions[1] = new AutonomousSample_ColorSensor(hardware, "Reading Color");
//
//        runFunctions(functions);    //run the two functions
//
//        LoggingController.getCurrentInstance().showLog("Blue value:", "%d", ((AutonomousSample_ColorSensor)functions[1]).blueValue);



        //Autonomous mode completed :))
        isAutonomousFinished = true;
    }

    /**
     * The runFunctions() method creates an AutonomousController that runs the given
     * AutonomousFunctions and takes care about their execution step by step
     */
    private void runFunctions(AutonomousAction[] functions)
    {
        AutonomousController controller = new AutonomousController(functions);
        try {
            controller.runAllFunctions();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
